package basic.hw3.part1.task8;

public class Main {
    public static void main(String[] args) {
        Atm atm1 = new Atm(62.50);
        System.out.println(atm1.usdToRub(1));
        System.out.println(atm1.rubToUsd(1));
        System.out.println(Atm.getCountOfExchanges());
        
        Atm atm2 = new Atm(70.20);
        System.out.println(atm2.rubToUsd(100));
        System.out.println(atm2.usdToRub(100));
        System.out.println(Atm.getCountOfExchanges());
    }
}