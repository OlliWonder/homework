package basic.hw3.part1.task8;

public class Atm {
    private static int countOfExchanges;
    private double course;
    
    public Atm(double course) {
        // Валидация данных.
        if (course <= 0) {
            System.out.println("Ошибка: некорректный курс. Повторите ввод.");
            System.exit(1);
        }
        else {
            this.course = course;
            countOfExchanges++;
        }
    }
    
    /**
     * Метод конвертирует рубли в доллары.
     */
    public double rubToUsd(int rubles) {
        return rubles / course;
    }
    
    /**
     * Метод конвертирует доллары в рубли.
     */
    public double usdToRub(int dollars) {
        return dollars * course;
    }
    
    /**
     * Метод возвращает количество совершенных конвертаций валют.
     */
    public static int getCountOfExchanges() {
        return countOfExchanges;
    }
}
