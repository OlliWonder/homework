package basic.hw3.part1.task4;

public class Main {
    public static void main(String[] args) {
        TimeUnit time = new TimeUnit(23, 35, 40);
    
        System.out.println("Начальное время:");
        time.getTime24();
        time.getTime12();
        
        time.increaseTime(48, 50, 80);
    
        System.out.println("Время после увеличения:");
        time.getTime24();
        time.getTime12();
    }
}
