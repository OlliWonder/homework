package basic.hw3.part1.task4;

public class TimeUnit {
    private int hours;
    private int minutes;
    private int seconds;
    
    public TimeUnit(int hours) {
        // Валидация данных
        if (hours >= 24 || hours < 0) {
            System.out.println("Ошибка: некорректные часы. Повторите ввод.");
            System.exit(1);
        }
        else {
            this.hours = hours;
        }
        minutes = 0;
        seconds = 0;
    }
    
    public TimeUnit(int hours, int minutes) {
        this(hours);
        
        if (minutes >= 60 || minutes < 0) {
            System.out.println("Ошибка: некорректные минуты. Повторите ввод.");
            System.exit(1);
        }
        else {
            this.minutes = minutes;
        }
    }
    
    public TimeUnit(int hours, int minutes, int seconds) {
        this(hours, minutes);
        
        if (seconds >= 60 || seconds < 0) {
            System.out.println("Ошибка: некорректные секунды. Повторите ввод.");
            System.exit(1);
        }
        else {
            this.seconds = seconds;
        }
    }
    
    /**
     * Метод выводит на эран время в 24-часов формате: hh:mm:ss
     */
    public void getTime24() {
        int[] timeLine = new int[6];
        timeLine[0] = hours / 10;
        timeLine[1] = hours % 10;
        timeLine[2] = minutes / 10;
        timeLine[3] = minutes % 10;
        timeLine[4] = seconds / 10;
        timeLine[5] = seconds % 10;
    
        for (int i = 0; i < 6; i++) {
            System.out.print(i == 1 || i == 3 ? timeLine[i] + ":" : timeLine[i]);
        }
        System.out.println();
    }
    
    /**
     * Метод выводит на эран время в 12-часовом формате: hh:mm:ss am(pm)
     */
    public void getTime12() {
        if (hours >= 0 && hours < 12) {
            if (hours == 0) {
                System.out.print(12 + ":");
            }
            else {
                System.out.print(hours > 9 ? (hours + ":") : ("0" + hours + ":"));
            }
            System.out.print(minutes > 9 ? (minutes + ":") : ("0" + minutes + ":"));
            System.out.println(seconds > 9 ? (seconds + " am") : ("0" + seconds + " am"));
        }
        else if (hours >= 12 && hours < 24) {
            if (hours == 12) {
                System.out.print(12 + ":");
            }
            else {
                int newHours = hours - 12;
                System.out.print(newHours > 9 ? (newHours + ":") : ("0" + newHours + ":"));
            }
            System.out.print(minutes > 9 ? (minutes + ":") : ("0" + minutes + ":"));
            System.out.println(seconds > 9 ? (seconds + " pm") : ("0" + seconds + " pm"));
        }
    }
    
    /**
     * Метод прибавдяет переданное время к установленному и приводит его к корректному виду.
     */
    public void increaseTime(int hours, int minutes, int seconds) {
        this.hours += hours;
        this.minutes += minutes;
        this.seconds += seconds;
        
        if (this.seconds >= 60) {
            // Прибавляем целые минуты, получившиеся из секунд, к общим минутам.
            this.minutes += this.seconds / 60;
            
            // Присваиваем остаток секундам. Далее повторим операции для минут и часов.
            this.seconds %= 60;
        }
        if (this.minutes >= 60) {
            this.hours += this.minutes / 60;
            this.minutes %= 60;
        }
        if (this.hours >= 24) {
            this.hours %= 24;
        }
    }
}
