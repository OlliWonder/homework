package basic.hw3.part1.task7;

public class TriangleChecker {
    private TriangleChecker() {
    }
    
    /**
     * Метод проверяет, можно ли построить треугольник из введнных сторон.
     * Каждая сторона должна быть меньше суммы двуг других сторон.
     */
    public static boolean isTriangle(double side1, double side2, double side3) {
        return side1 + side2 > side3 && side1 + side3 > side2 && side2 + side3 > side1;
    }
}
