package basic.hw3.part1.task7;

public class Main {
    public static void main(String[] args) {
        System.out.println(TriangleChecker.isTriangle(5.2, 4.5, 3.8));
        System.out.println(TriangleChecker.isTriangle(10.2, 4.5, 3.8));
        System.out.println(TriangleChecker.isTriangle(1000.1, 500.1, 500.0));
        System.out.println(TriangleChecker.isTriangle(1000.1, 500.1, 500.5));
        System.out.println(TriangleChecker.isTriangle(1, 2, 2.99));
    }
}
