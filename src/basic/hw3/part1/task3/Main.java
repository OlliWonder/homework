package basic.hw3.part1.task3;

import basic.hw3.part1.task2.Student;

public class Main {
    public static void main(String[] args) {
        
        Student student1 = new Student("Иван", "Пиванов");
        Student student2 = new Student("Петр", "Петров");
        Student student3 = new Student("Сидор", "Асидоров");
        
        int[] grades1 = {1, 2, 3, 4};
        student1.setGrades(grades1);
        int[] grades2 = {3, 4, 5, 3, 4, 5};
        student2.setGrades(grades2);
        int[] grades3 = {5, 4, 5, 5, 4, 5, 5};
        student3.setGrades(grades3);
        
        Student[] students = {student1, student2, student3};
        Student best = StudentService.bestStudent(students);
        System.out.println("Лучший студент: " + best.getSurname() + " " + best.getName() + " " + best.averageGrade());
        
        StudentService.sortBySurname(students);
    
        System.out.println("Фамилии студентов в отсортированном массиве:");
        for (int i = 0; i < students.length; i++) {
            System.out.println(students[i].getSurname());
        }
    }
}
