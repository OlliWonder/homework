package basic.hw3.part1.task3;

import basic.hw3.part1.task2.Student;

import java.util.Arrays;

public class StudentService {
    
    private StudentService() {
    }
    
    /**
     * Метод находит максимальную среднюю оценку у студента в массива студентов и возвращает лучшего студента.
     */
    public static Student bestStudent(Student[] students) {
        double temp = students[0].averageGrade();
        int index = 0;
        
        for (int i = 1; i < students.length; i++) {
            if  (temp < students[i].averageGrade()) {
                temp = students[i].averageGrade();
                index = i;
            }
        }
        return students[index];
    }
    
    /**
     * Метод сортирует массив студентов (фамилии в алфавитном порядке) через массив фамилий.
     */
    public static void sortBySurname(Student[] students) {
        // Создаем массив фамилий длиной, равной длине массива студентов
        String[] surnames = new String[students.length];
        
        // Заполняем массив фамилиями из массива студентов
        for (int i = 0; i < surnames.length; i++) {
            surnames[i] = students[i].getSurname();
        }
        
        // Сортируем массив фамилий
        Arrays.sort(surnames);
        
        // Сравниваем фамилии в отсортированном массив фамилий с фамилиями у студентов в массиве студентов.
        // При совпадении меняем студентов местами.
        Student temp;
        
        for (int i = 0; i < surnames.length; i++) {
            for (int j = 0; j < surnames.length; j++) {
                if (surnames[i].equals(students[j].getSurname())) {
                    temp = students[i];
                    students[i] = students[j];
                    students[j] = temp;
                    break;
                }
            }
        }
    }
}
