package basic.hw3.part1.task1;

public class Cat {
    public Cat() {
    }
    
    private void sleep() {
        System.out.println("Sleep");
    }
    
    private void meow() {
        System.out.println("Meow");
    }
    
    private void eat() {
        System.out.println("Eat");
    }
    
    public void status() {
        int n = (int)(Math.random() * 3);
        switch (n) {
            case 0 -> sleep();
            case 1 -> meow();
            case 2 -> eat();
        }
    }
}