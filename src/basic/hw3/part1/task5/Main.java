package basic.hw3.part1.task5;

public class Main {
    public static void main(String[] args) {
        DayOfWeek[] daysOfWeek = new DayOfWeek[7];
        for (int i = 0; i < daysOfWeek.length; i++) {
            daysOfWeek[i] = new DayOfWeek(i + 1);
        }
    
        for (DayOfWeek dayOfWeek : daysOfWeek) {
            dayOfWeek.getDayOfWeek();
        }
    }
}