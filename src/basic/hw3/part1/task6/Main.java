package basic.hw3.part1.task6;

public class Main {
    public static void main(String[] args) {
        AmazingString amazingString1 = new AmazingString(new char[]{' ', ' ', 'H', 'e', 'l', 'l', 'o', '!'});
        AmazingString amazingString2 = new AmazingString("   Pick-a-boo!");
    
        System.out.println(amazingString1.getChar(3));
        System.out.println(amazingString2.getChar(3));
    
        System.out.println(amazingString1.getLength());
        System.out.println(amazingString2.getLength());
        
        amazingString1.printString();
        amazingString2.printString();
        
        System.out.println(amazingString1.isSubstring(new char[]{'e', 'l', 'l'}));
        System.out.println(amazingString2.isSubstring("boo!"));
        
        amazingString1.deleteLeadingSpaces();
        amazingString1.printString();
        amazingString2.deleteLeadingSpaces();
        amazingString2.printString();
        
        amazingString1.reverseString();
        amazingString1.printString();
        amazingString2.reverseString();
        amazingString2.printString();
    }
}
