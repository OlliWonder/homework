package basic.hw3.part1.task6;

public class AmazingString {
    private char[] letters;
    
    public AmazingString(char[] letters) {
        this.letters = letters;
    }
    
    public AmazingString(String text) {
        letters = new char[text.length()];
        // Преобразуем строку в массив символов (согласно условию задачи).
        for (int i = 0; i < text.length(); i++) {
            letters[i] = text.charAt(i);
        }
    }
    
    /**
     * Метод возвращает i-й символ в массиве.
     */
    public char getChar(int index) {
        return letters[index];
    }
    
    /**
     * Метод возвращает длину массива.
     */
    public int getLength() {
        return letters.length;
    }
    
    /**
     * Метод печтатет массив символов.
     */
    public void printString() {
        for (char letter : letters) {
            System.out.print(letter);
        }
        System.out.println();
    }
    
    /**
     * Метод проверяет, есть ли введееная "подсторока" из массива символов в объекте (массиве символов).
     * Ищет совпадение по первому символу, если находит, проверяет совпадение всех оставшихся.
     */
    public boolean isSubstring(char[] input) {
        int i = 0;
        
        while (input[0] != letters[i]) {
            i++;
            if (i == letters.length) {
                return false;
            }
        }
        
        for (int j = 1; j < letters.length - input.length && j < input.length; j++) {
            if (input[j] != letters[j + i]) {
                return false;
            }
        }
        return true;
    }
    
    /**
     * Метод проверяет, есть ли введенная подстрока в массиве символов.
     * Переводит введеную строку в массив символов.
     * Ищет совпадение по первому символу, если находит, проверяет совпадение всех оставшихся.
     */
    public boolean isSubstring(String input) {
        char[] fromInput = new char[input.length()];
        
        for (int i = 0; i < input.length(); i++) {
            fromInput[i] = input.charAt(i);
        }
    
        int i = 0;
        
        while (fromInput[0] != letters[i]) {
            i++;
            if (i == letters.length) {
                return false;
            }
        }
        
        for (int j = 1; j < letters.length - fromInput.length && j < fromInput.length; j++) {
            if (fromInput[j] != letters[j + i]) {
                return false;
            }
        }
        return true;
    }
    
    /**
     * Метод считает количество лидирующих пробелов в массиве, создаёт новый массив без пробелов, меньшей длины.
     * Затем присваивает ссылку на новый массив исходному.
     */
    public void deleteLeadingSpaces() {
        int countOfSpaces = 0;
        
        for (int i = 0; i < letters.length; i++) {
            if (letters[i] == ' ') {
                countOfSpaces++;
            }
            else {
                break;
            }
        }
    
        char[] temp = new char[letters.length - countOfSpaces];
        
        for (int i = 0; i < temp.length; i++) {
            temp[i] = letters[i + countOfSpaces];
        }
        letters = temp;
    }
    
    /**
     * Метод переворачивает массив (первый элемент становится прследним и т.д.).
     */
    public void reverseString() {
        char[] reverse = new char[letters.length];
        
        for (int i = 0, j = letters.length - 1; i < letters.length && j >= 0; i++, j--) {
            reverse[i] = letters[j];
        }
        letters = reverse;
    }
}