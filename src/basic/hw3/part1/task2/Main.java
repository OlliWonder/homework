package basic.hw3.part1.task2;

import java.util.Arrays;

public class Main {
    public static void main(String[] args) {
        Student student = new Student();
        
        student.setName("Иван");
        student.setSurname("Иванов");
        int[] grades = {1, 2, 3, 4, 5, 5, 4, 3, 2, 1};
        student.setGrades(grades);
        
        System.out.println("Имя: " + student.getName());
        System.out.println("Фамилия: " + student.getSurname());
        System.out.print("Оценки до внесения изменений: ");
        System.out.println(Arrays.toString(student.getGrades()));
        
        System.out.println("Средний балл до внесения изменений: " + student.averageGrade());
        
        student.addGrades(5);
        
        System.out.print("Оценки после внесения изменений: ");
        System.out.println(Arrays.toString(student.getGrades()));
        
        System.out.println("Средний балл после внесения изменений: " + student.averageGrade());
    
    }
}
