package basic.hw3.part2whitharray;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

class Book {
    private final String title;
    private final String author;
    private List<Integer> grades = new ArrayList<>();
    private boolean isEnable;
    
    public Book(String title, String author) {
        this.title = title;
        this.author = author;
        isEnable = true;
    }
    
    public String getTitle() {
        return title;
    }
    
    public String getAuthor() {
        return author;
    }
    
    public List<Integer> getGrades() {
        return grades;
    }
    
    public void addGrade(int grade) {
        grades.add(grade);
    }
    
    public double getAverageGrade() {
        double temp = 0;
        for (int i = 0; i < grades.size(); i++) {
            temp += grades.get(i);
        }
        return (int)(temp / grades.size() * 10) / 10.0;
    }
    
    public boolean isEnable() {
        return isEnable;
    }
    
    public void setEnable(boolean enable) {
        isEnable = enable;
    }
    
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Book book = (Book) o;
        return title.equals(book.title);
    }
    
    @Override
    public int hashCode() {
        return Objects.hash(title);
    }
    
    @Override
    public String toString() {
        return "Книга: " + author + " " + title;
    }
}
