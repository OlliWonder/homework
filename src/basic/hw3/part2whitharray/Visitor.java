package basic.hw3.part2whitharray;

import java.util.Objects;

class Visitor {
    private final String name;
    private int id = 0;
    private static int idCount = 0;
    private boolean isEmpty;
    private Book whatIsBook = null;
    
    public Visitor(String name) {
        this.name = name;
        isEmpty = true;
    }
    
    public String getName() {
        return name;
    }
    
    public int getId() {
        return id;
    }
    
    public void setId() {
        idCount++;
        id = idCount;
    }
    
    public boolean isEmpty() {
        return isEmpty;
    }
    
    public void setEmpty(boolean empty) {
        isEmpty = empty;
    }
    
    public Book getWhatIsBook() {
        return whatIsBook;
    }
    
    public void setWhatIsBook(Book whatIsBook) {
        this.whatIsBook = whatIsBook;
    }
    
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Visitor visitor = (Visitor) o;
        return name.equals(visitor.name);
    }
    
    @Override
    public int hashCode() {
        return Objects.hash(name);
    }
    
    @Override
    public String toString() {
        return "Посетитель: " + name;
    }
}
