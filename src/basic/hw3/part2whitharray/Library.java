package basic.hw3.part2whitharray;

import java.util.ArrayList;
import java.util.List;

class Library {
    private List<Book> books = new ArrayList<>();
    private List<Visitor> visitors = new ArrayList<>();
    
    public void addBook(Book book) {
        if (!books.contains(book)) {
            books.add(book);
            System.out.println(book.toString() + " добавлена в библиотеку");
        }
        else {
            System.out.println(book.toString() + " уже есть в библиотеке");
        }
    }
    
    public void addVisitor(Visitor visitor) {
        if (!visitors.contains(visitor)) {
            visitors.add(visitor);
            System.out.println(visitor.toString() + " успешно зарегистрирован");
        }
        else {
            System.out.println(visitor.toString() + " зарегистрирован ранее");
        }
    }
    
    public List<Book> getBooks() {
        return books;
    }
    
    public List<Visitor> getVisitors() {
        return visitors;
    }
    
    public void deleteBook(String title) {
        for (int i = 0; i < books.size(); i++) {
            if (books.get(i).getTitle().equals(title) && books.get(i).isEnable()) {
                System.out.println((books.get(i)).toString() + " удалена из библиотеки");
                books.remove(i);
                return;
            }
        }
        System.out.println("Невозможно удалить книгу");
    }
    
    public Book getBookByTitle(String title) {
        for (int i = 0; i < books.size(); i++) {
            if (books.get(i).getTitle().equals(title)) {
                return books.get(i);
            }
        }
        return null;
    }
    
    public ArrayList<Book> getListOfBooksByAuthor(String author) {
        ArrayList<Book> booksByAuthor = new ArrayList<>();
        for (int i = 0; i < books.size(); i++) {
            if (books.get(i).getAuthor().equals(author)) {
                booksByAuthor.add(books.get(i));
            }
        }
        return booksByAuthor;
    }
    
    public void giveBook(Book book, Visitor visitor) {
        if (books.contains(book) && book.isEnable() && visitor.isEmpty()) {
            book.setEnable(false);
            visitor.setEmpty(false);
            visitor.setWhatIsBook(book);
            System.out.println(book.toString() + " выдана " + visitor.toString());
            
            if (visitor.getId() == 0) {
                visitor.setId();
            }
        }
        else {
            System.out.println("Невозможно выдать " + book.toString());
        }
    }
    
    public void takeBook(Book book, Visitor visitor, int grade) {
        if (book.equals(visitor.getWhatIsBook())) {
            book.setEnable(true);
            visitor.setEmpty(true);
            visitor.setWhatIsBook(null);
            book.addGrade(grade);
            System.out.println(book.toString() + " возвращена от " + visitor.toString());
        }
        else {
            System.out.println("Невозможно вернуть " + book.toString());
        }
    }
    
    public double getAverageGradeByTitle(String title) {
        for (int i = 0; i < books.size(); i++) {
            if (books.get(i).getTitle().equals(title)) {
                return books.get(i).getAverageGrade();
            }
        }
        return 0.0;
    }
}
