package basic.hw3.part2whitharray;

import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        Library library = new Library();
        
        Book book1 = new Book("Мастер и Маргарита", "Булгаков");
        Book book2 = new Book("Золотая рыбка", "Пушкин");
        Book book3 = new Book("Алые паруса", "Грин");
        Book book4 = new Book("Балда", "Пушкин");
        Book book5 = new Book("Руслан и Людмила", "Пушкин");
        
        library.addBook(book1);
        library.addBook(book2);
        library.addBook(book3);
        library.addBook(book4);
        library.addBook(book5);
        System.out.println();
    
        library.addBook(new Book("Золотая рыбка", "Пушкин"));
        
        library.deleteBook("Балда");
        System.out.println();
        
        for (int i = 0; i < library.getBooks().size(); i++) {
            System.out.println(library.getBooks().get(i));
        }
        System.out.println();
        
        System.out.println(library.getBookByTitle("Мастер и Маргарита").toString());
        System.out.println();
    
        List<Book> myList = new ArrayList<>();
        myList = library.getListOfBooksByAuthor("Пушкин");
    
        for (int i = 0; i < myList.size(); i++) {
            System.out.println(myList.get(i));
        }
        System.out.println();
        
        Visitor visitor1 = new Visitor("Иванов");
        Visitor visitor2 = new Visitor("Петров");
        Visitor visitor3 = new Visitor("Сидоров");
        
        library.addVisitor(visitor1);
        library.addVisitor(visitor2);
        library.addVisitor(visitor3);
        System.out.println();
        
        library.giveBook(book3, visitor1);
        library.giveBook(book2, visitor1);
        library.giveBook(book3, visitor2);
        
        library.takeBook(book3, visitor2,5);
        library.takeBook(book3, visitor1, 5);
        System.out.println("Средняя оценка " + book3.toString() + ": " + book3.getAverageGrade() + "\n");
    
        library.giveBook(book3, visitor2);
        library.takeBook(book3, visitor2, 4);
        System.out.println("Средняя оценка " + book3.toString() + ": " + book3.getAverageGrade() + "\n");
        
        library.giveBook(book3, visitor3);
        library.takeBook(book3, visitor3, 3);
        System.out.println("Средняя оценка " + book3.toString() + ": " + book3.getAverageGrade() + "\n");
    }
}
