package basic.hw3.part2;
import java.util.Scanner;
class Library {
    private Book[] books = new Book[Book.getCount()];
    private Visitor[] visitors = new Visitor[Visitor.getCount()];
    
    public Library() {
    }
    
    public void addBook(String name, String author) {
        if (getIndexOfBookByName(name) > -1) {
            System.out.println("Ошибка добавления книги!\nКнига с названием: " +
                    name + " - уже есть в библиотеке.");
            return;
        }
        else {
            Book[] temp = new Book[books.length + 1];
            System.arraycopy(books, 0, temp, 0, books.length);
            temp[temp.length - 1] = new Book(name, author);
            books = temp;
            System.out.println("Книга: " + name + ", автор: " + author + " - добавлена в библиотеку.");
        }
    }
    
    public void addVisitor(String name) {
        if (getIndexOfVisitorByName(name) > -1) {
            System.out.println("Ошибка добавления посетителя!\nПосетитель с именем: " +
                    name + " - уже есть в библиотеке.");
            return;
        }
        else {
            Visitor[] temp = new Visitor[visitors.length + 1];
            System.arraycopy(visitors, 0, temp, 0, visitors.length);
            temp[temp.length - 1] = new Visitor(name);
            visitors = temp;
            System.out.println("Посетитель: " + name + " - добавлен в библиотеку.");
        }
    }
    
    public void removeBook(String name) {
        int index = getIndexOfBookByName(name);
        
        if (index > -1  && !books[index].isEnable()) {
            System.out.println("Ошибка удаления книги!\nКнига с названием: " +
                    name + " одолжена.");
        }
        else if (index == -1) {
            System.out.println("Ошибка удаления книги!\nКнига с названием: " +
                    name + " не зарегистрирована в библиотеке.");
        }
        else if (index > - 1 && books[index].isEnable()) {
            
            Book[] temp = new Book[books.length - 1];
            
            if (index > 0 && index < books.length - 1) {
                System.arraycopy(books, 0, temp, 0, index);
                System.arraycopy(books, index + 1, temp, index, books.length - 1 - index);
            }
            else if (index == 0) {
                System.arraycopy(books, 1, temp, 0, books.length - 1);
            }
            else if (index == books.length - 1) {
                System.arraycopy(books, 0, temp, 0, books.length - 1);
            }
            books = temp;
            Book.decreaseCount();
            System.out.println("Книга с названием: " + name + " - успешно удалена.");
        }
    }
    
    public Book getBookByName(String name) {
        if (getIndexOfBookByName(name) == -1) {
            System.out.println("Книга с названием: " + name + " не найдена в библиотеке.");
            return null;
        }
        else {
            return books[getIndexOfBookByName(name)];
        }
    }
    
    public Book[] getBooksByAuthor(String author) {
        int count = 0;
        for (Book book : books) {
            if (book.getAuthor().equals(author)) {
                count++;
            }
        }
        
        Book[] sameAuthor = new Book[count];
        int i = 0;
        for (int j = 0; j < books.length; j++) {
            if (books[j].getAuthor().equals(author)) {
                sameAuthor[i] = books[j];
                i++;
            }
        }
        return sameAuthor;
    }
    
    public void lendOutBook(String nameOfBook, String nameOfVisitor) {
        int idxOfBook = getIndexOfBookByName(nameOfBook);
        int idxOfVisitor = getIndexOfVisitorByName(nameOfVisitor);
        
        if (idxOfBook > -1 && books[idxOfBook].isEnable() && idxOfVisitor > -1 && visitors[idxOfVisitor].isEmpty()) {
            books[idxOfBook].setEnable(false);
            visitors[idxOfVisitor].setEmpty(false);
            
            if (visitors[idxOfVisitor].getId() == 0) {
                visitors[idxOfVisitor].setId();
            }
            
            books[idxOfBook].setWhoHasBook(visitors[idxOfVisitor]);
            visitors[idxOfVisitor].setWhatIsBook(books[idxOfBook]);
            System.out.println("Книга " + nameOfBook + " успешно выдана " + nameOfVisitor);
        }
        else if (idxOfBook == -1) {
            System.out.println("Книга с названием: " + nameOfBook + " не зарегистрирована в библиотеке.");
        }
        else if (!visitors[idxOfVisitor].isEmpty()) {
            System.out.println("У посетителя " + nameOfVisitor + " уже есть книга.");
        }
        else if (!books[idxOfBook].isEnable()) {
            System.out.println("Книга " + nameOfBook + " занята.");
            
        }
    }
    
    public void returnBook(String nameOfBook, String nameOfVisitor) {
        int idxOfBook = getIndexOfBookByName(nameOfBook);
        int idxOfVisitor = getIndexOfVisitorByName(nameOfVisitor);
        
        if (!visitors[idxOfVisitor].isEmpty() && visitors[idxOfVisitor].getWhatIsBook().equals(books[idxOfBook])) {
            books[idxOfBook].setEnable(true);
            visitors[idxOfVisitor].setEmpty(true);
            books[idxOfBook].setWhoHasBook(null);
            visitors[idxOfVisitor].setWhatIsBook(null);
    
            System.out.println("Книга " + nameOfBook + " успешно возвращена.\n" +
                    "Оцените прочитанную книгу от 1 до 5: ");
            int grade = new Scanner(System.in).nextInt();
            books[idxOfBook].setAverageGrade(grade);
        }
        else {
            System.out.println("Ошибка возврата книги!\nКнига с названием " + nameOfBook +
                    " была взята другим посетителем.");
        }
    }
    
    public double getAverageGradeOfBook(String name) {
        return books[getIndexOfBookByName(name)].getAverageGrade();
    }
    
    private int getIndexOfBookByName(String name) {
        for (int i = 0; i < books.length; i++) {
            if (books[i].getName().equals(name)) {
                return i;
            }
        }
        return -1;
    }
    
    private int getIndexOfVisitorByName(String name) {
        for (int i = 0; i < visitors.length; i++) {
            if (visitors[i].getName().equals(name)) {
                return i;
            }
        }
        return -1;
    }
}
