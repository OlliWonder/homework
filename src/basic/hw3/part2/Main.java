package basic.hw3.part2;

public class Main {
    public static void main(String[] args) {
        Library library = new Library();
        library.addBook("Евгений Онегин", "Пушкин");
        library.addBook("Преступление и наказание", "Достоевский");
        library.addBook("Руслан и Людмила", "Пушкин");
        library.addBook("Горе от ума", "Грибоедов");
        library.addBook("Мастер и Маргарита", "Булгаков");
        library.addBook("Евгений Онегин", "Пушкин");
    
        library.removeBook("Горе от ума");
        library.removeBook("1000 и 1 ночь");
    
        Book find = library.getBookByName("Мастер и Маргарита");
        System.out.println("Найдена книга: " + find.getName());
    
        Book find2 = library.getBookByName("Собачье сердце");
    
        Book[] find3 = library.getBooksByAuthor("Пушкин");
        for (Book book : find3) {
            System.out.println(book.getName());
        }
    
        library.addVisitor("Иванов");
        library.addVisitor("Петров");
        library.addVisitor("Сидоров");
    
        library.lendOutBook("Евгений Онегин", "Иванов");
        library.lendOutBook("Евгений Онегин", "Петров");
        library.lendOutBook("Руслан и Людмила", "Иванов");
    
        library.returnBook("Евгений Онегин", "Петров");
        library.returnBook("Евгений Онегин", "Иванов");
    
        System.out.println(library.getAverageGradeOfBook("Евгений Онегин"));
    
        library.lendOutBook("Евгений Онегин", "Петров");
        library.returnBook("Евгений Онегин", "Петров");
    
        System.out.println(library.getAverageGradeOfBook("Евгений Онегин"));
    }
}
