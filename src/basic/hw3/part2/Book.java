package basic.hw3.part2;

class Book {
    private String name;
    private String author;
    private boolean isEnable;
    private static int count = 0;
    private Visitor whoHasBook = null;
    private int[] grades;
    private double averageGrade = 0.0;
    private int countOfGrades = 0;
    
    protected Book(String name, String author) {
        this.name = name;
        this.author = author;
        isEnable = true;
        count++;
        grades = new int[countOfGrades];
    }
    
    public String getName() {
        return name;
    }
    
    public String getAuthor() {
        return author;
    }
    
    public static int getCount() {
        return count;
    }
    
    public Visitor getWhoHasBook() {
        return whoHasBook;
    }
    
    public double getAverageGrade() {
        return averageGrade;
    }
    
    public boolean isEnable() {
        return isEnable;
    }
    
    public void setEnable(boolean enable) {
        isEnable = enable;
    }
    
    public void setWhoHasBook(Visitor whoHasBook) {
        this.whoHasBook = whoHasBook;
    }
    
    public void setAverageGrade(int grade) {
        countOfGrades++;
        int[] temp = new int[countOfGrades];
        System.arraycopy(grades, 0, temp, 0, countOfGrades - 1);
        temp[temp.length - 1] = grade;
        grades = temp;
        
        double sumOfGrades = 0.0;
        for (int i = 0; i < grades.length; i++) {
            sumOfGrades += grades[i];
        }
        averageGrade = ((int)(sumOfGrades / countOfGrades * 10)) / 10.0;
    }
    
    public static void decreaseCount() {
        count--;
    }
}
