package basic.hw3.part2;

class Visitor {
    private String name;
    private static int idCount = 0;
    private int id;
    private boolean isEmpty;
    private static int count = 0;
    private Book whatIsBook = null;
    
    public Visitor(String name) {
        this.name = name;
        isEmpty = true;
        count++;
    }
    
    public String getName() {
        return name;
    }
    
    public int getId() {
        return id;
    }
    
    public static int getCount() {
        return count;
    }
    
    public Book getWhatIsBook() {
        return whatIsBook;
    }
    
    public boolean isEmpty() {
        return isEmpty;
    }
    
    public void setId() {
        id = ++idCount;
    }
    
    public void setEmpty(boolean empty) {
        isEmpty = empty;
    }
    
    public void setWhatIsBook(Book whatIsBook) {
        this.whatIsBook = whatIsBook;
    }
}
