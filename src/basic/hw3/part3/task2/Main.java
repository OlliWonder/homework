package basic.hw3.part3.task2;

public class Main {
    public static void main(String[] args) {
        Furniture furniture1 = new Tabouret();
        Furniture furniture2 = new Furniture();
        Furniture furniture3 = new Table();
        
        System.out.println(BestCarpenterEver.isFix(furniture1));
        System.out.println(BestCarpenterEver.isFix(furniture2));
        System.out.println(BestCarpenterEver.isFix(furniture3));
    }
}
