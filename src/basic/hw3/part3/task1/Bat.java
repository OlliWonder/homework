package basic.hw3.part3.task1;

public final class Bat extends Mammal implements Flying {
    
    @Override
    public void fly() {
        System.out.println("Летает медленно");
    }
}
