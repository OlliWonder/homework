package basic.hw3.part3.task1;

public final class GoldFish extends Fish implements Swimming {
    
    @Override
    public void swim() {
        System.out.println("Плавает медленно");
    }
}
