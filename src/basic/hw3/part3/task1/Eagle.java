package basic.hw3.part3.task1;

public final class Eagle extends Bird implements Flying {
    
    @Override
    public void fly() {
        System.out.println("Летает быстро");
    }
}
