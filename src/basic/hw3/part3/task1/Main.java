package basic.hw3.part3.task1;

public class Main {
    public static void main(String[] args) {
        Bat bat = new Bat();
        Dolphin dolphin = new Dolphin();
        GoldFish goldFish = new GoldFish();
        Eagle eagle = new Eagle();
    
        System.out.println("Летучая мышь:");
        bat.eat();
        bat.sleep();
        bat.wayOfBirth();
        bat.fly();
    
        System.out.println("\nДельфин:");
        dolphin.eat();
        dolphin.sleep();
        dolphin.wayOfBirth();
        dolphin.swim();
    
        System.out.println("\nЗолотая рыбка:");
        goldFish.eat();
        goldFish.sleep();
        goldFish.wayOfBirth();
        goldFish.swim();
    
        System.out.println("\nОрел:");
        eagle.eat();
        eagle.sleep();
        eagle.wayOfBirth();
        eagle.fly();
    }
}
