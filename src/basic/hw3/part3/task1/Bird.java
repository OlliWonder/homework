package basic.hw3.part3.task1;

public abstract class Bird extends Animal {
    
    @Override
    protected void wayOfBirth() {
        System.out.println("Откладывает яйца");
    }
}
