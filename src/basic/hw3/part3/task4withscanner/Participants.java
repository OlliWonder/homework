package basic.hw3.part3.task4withscanner;

import java.util.ArrayList;
import java.util.Scanner;

public class Participants {
    private static int countOfParticipants;
    private static ArrayList<String> owners = new ArrayList<>();
    private static ArrayList<String> dogs = new ArrayList<>();
    private static ArrayList<Double> grades = new ArrayList<>();
    
    public Participants() {
        Scanner input = new Scanner(System.in);
        countOfParticipants = input.nextInt();
    
        for (int i = 0; i < countOfParticipants; i++) {
            owners.add(input.next());
        }
    
        for (int i = 0; i < countOfParticipants; i++) {
            dogs.add(input.next());
        }
        
        double sum;
        for (int i = 0; i < countOfParticipants; i++) {
            sum = 0;
            for (int j = 0; j < 3; j++) { // 3 - три судьи
                sum += input.nextInt();
            }
            grades.add((int)(sum / 3 * 10) / 10.0);
        }
        input.close();
    }
    
    public static String getOwner(int number) {
        return owners.get(number - 1);
    }
    
    public static String getDog(int number) {
        return dogs.get(number - 1);
    }
    
    public static double getGrade(int number) {
        return grades.get(number - 1);
    }
    
    public static void winners() {
        double maxGrade1 = grades.get(0);
        int index1 = 0;
        for (int i = 1; i < countOfParticipants; i++) {
            if (maxGrade1 < grades.get(i)) {
                maxGrade1 = grades.get(i);
                index1 = i;
            }
        }
        System.out.println(owners.get(index1) + ": " + dogs.get(index1) + ", " + maxGrade1);
    
        double maxGrade2 = grades.get(0);
        int index2 = 0;
        for (int i = 1; i < countOfParticipants; i++) {
            if (maxGrade2 < grades.get(i) && i != index1) {
                maxGrade2 = grades.get(i);
                index2 = i;
            }
        }
        System.out.println(owners.get(index2) + ": " + dogs.get(index2) + ", " + maxGrade2);
    
        double maxGrade3 = grades.get(0);
        int index3 = 0;
        for (int i = 1; i < countOfParticipants; i++) {
            if (maxGrade3 < grades.get(i) && i != index1 && i != index2) {
                maxGrade3 = grades.get(i);
                index3 = i;
            }
        }
        System.out.println(owners.get(index3) + ": " + dogs.get(index3) + ", " + maxGrade3);
        
    }
}
