package basic.hw3.part3.task4withscanner;

public class Grade {
    private double averageGrade;
    
    public Grade(int number) {
        averageGrade = Participants.getGrade(number);
    }
}
