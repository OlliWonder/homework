package basic.hw3.part3.task4withoutscanner;

public class Main {
    public static void main(String[] args) {
        Owner owner1 = new Owner("Иван");
        Owner owner2 = new Owner("Николай");
        Owner owner3 = new Owner("Анна");
        Owner owner4 = new Owner("Дарья");
        
        Dog dog1 = new Dog("Жучка");
        Dog dog2 = new Dog("Кнопка");
        Dog dog3 = new Dog("Цезарь");
        Dog dog4 = new Dog("Добряш");
        
        Grade grade1 = new Grade(new int[]{7, 6, 7});
        Grade grade2 = new Grade(new int[]{8, 8, 7});
        Grade grade3 = new Grade(new int[]{4, 5, 6});
        Grade grade4 = new Grade(new int[]{9, 9, 9});
    
        System.out.println(grade1.getAverageGradeById(1));
        System.out.println(grade2.getAverageGradeById(2));
        System.out.println(grade3.getAverageGradeById(3));
        System.out.println(grade4.getAverageGradeById(4));
        
        Grade.getWinners();
    }
}
