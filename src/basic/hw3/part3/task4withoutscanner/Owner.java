package basic.hw3.part3.task4withoutscanner;

import java.util.ArrayList;

public class Owner {
    private static ArrayList<String> names = new ArrayList<>();
    private String name;
    private static int count = 0;
    private int id;
    
    public Owner(String name) {
        this.name = name;
        id = ++count;
        names.add(name);
    }
    
    public static String getNameById(int id) {
        return names.get(id - 1);
    }
}
