package basic.hw3.part3.task4withoutscanner;

import java.util.ArrayList;

public class Dog {
    private static ArrayList<String> nickNames = new ArrayList<>();
    private String nickName;
    private static int count = 0;
    private int id;
    
    public Dog(String nickName) {
        this.nickName = nickName;
        id = ++count;
        nickNames.add(nickName);
    }
    
    public static String getNickNameById(int id) {
        return nickNames.get(id - 1);
    }
}
