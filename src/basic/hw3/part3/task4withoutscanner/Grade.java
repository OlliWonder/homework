package basic.hw3.part3.task4withoutscanner;

import java.util.ArrayList;

public class Grade {
    private static ArrayList<int[]> allGrades = new ArrayList<>();
    private static ArrayList<Double> averageGrades = new ArrayList<>();
    private int[] grades;
    private static int count = 0;
    private int id;
    
    public Grade(int[] grades) {
        this.grades = grades;
        id = ++count;
        allGrades.add(grades);
    }
    
    public static int[] getGradesById(int id) {
        return allGrades.get(id - 1);
    }
    
    public double getAverageGradeById(int id) {
        double sum = 0;
        for (int i = 0; i < 3; i++) {
            sum += allGrades.get(id - 1)[i];
        }
        double averageGrade = (int)(sum / 3 * 10) / 10.0;
        averageGrades.add(averageGrade);
        return averageGrade;
    }
    
    public static void getWinners() {
        double maxGrade1 = averageGrades.get(0);
        int index1 = 0;
        for (int i = 1; i < count; i++) {
            if (maxGrade1 < averageGrades.get(i)) {
                maxGrade1 = averageGrades.get(i);
                index1 = i;
            }
        }
        System.out.println(Owner.getNameById(index1 + 1) + ": " + Dog.getNickNameById(index1 + 1) + ", " + maxGrade1);
    
        double maxGrade2 = averageGrades.get(0);
        int index2 = 0;
        for (int i = 1; i < count; i++) {
            if (maxGrade2 < averageGrades.get(i) && i != index1) {
                maxGrade2 = averageGrades.get(i);
                index2 = i;
            }
        }
        System.out.println(Owner.getNameById(index2 + 1) + ": " + Dog.getNickNameById(index2 + 1) + ", " + maxGrade2);
    
        double maxGrade3 = averageGrades.get(0);
        int index3 = 0;
        for (int i = 1; i < count; i++) {
            if (maxGrade3 < averageGrades.get(i) && i != index1 && i != index2) {
                maxGrade3 = averageGrades.get(i);
                index3 = i;
            }
        }
        System.out.println(Owner.getNameById(index3 + 1) + ": " + Dog.getNickNameById(index3 + 1) + ", " + maxGrade3);
    }
}
