package basic.hw3.part3.task3;

import java.util.Scanner;
import java.util.ArrayList;

public class Task3 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int n = input.nextInt();
        int m = input.nextInt();
        
        ArrayList<Integer> list = new ArrayList<>();
        int index = 0;
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                list.add(i + j);
                System.out.print(list.get(index) + " ");
                index++;
            }
            System.out.println();
        }
    }
}
