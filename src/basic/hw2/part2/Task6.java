package basic.hw2.part2;

import java.util.Scanner;

public class Task6 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int a = input.nextInt();
        int b = input.nextInt();
        int c = input.nextInt();
        int k = input.nextInt();

        int[][] week = new int[4][7];
        for (int i = 0; i < 7; i++) {
            for (int j = 0; j < 4; j++) {
                week[j][i] = input.nextInt();
            }
        }

        int aW = 0;
        int bW = 0;
        int cW = 0;
        int kW = 0;

        for (int j = 0; j < 7; j++) {
            aW += week[0][j];
            bW += week[1][j];
            cW += week[2][j];
            kW += week[3][j];
        }

        if (aW <= a && bW <= b && cW <= c && kW <= k) {
            System.out.println("Отлично");
        }
        else {
            System.out.println("Нужно есть поменьше");
        }
    }
}