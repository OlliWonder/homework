package basic.hw2.part2;

import java.util.Scanner;

public class Task4 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int n = input.nextInt();
        int[][] arr = new int[n][n];
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                arr[i][j] = input.nextInt();
            }
        }
        int p = input.nextInt();
        int x = 0, y = 0;
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                if (arr[i][j] == p) {
                    x = i;
                    y = j;
                }
            }
        }
        int[][] fin = new int[n - 1][n - 1];
        for (int i = 0, k = 0; i < n - 1 && k < n; i++, k++) {
            for (int j = 0, l = 0; j < n - 1 && l < n; j++, l++) {
                if (k == x) {
                    fin[i][j] = arr[k + 1][l];
                    k++;
                }
                else if (l == y) {
                    fin[i][j] = arr[k][l + 1];
                    l++;
                }
                else {
                    fin[i][j] = arr[k][l];
                }
            }
        }
        for (int i = 0; i < n - 1; i++) {
            for (int j = 0; j < n - 1; j++) {
                System.out.print(j != n - 2 ? fin[i][j] + " " : fin[i][j]);
            }
            System.out.println();
        }
    }
}