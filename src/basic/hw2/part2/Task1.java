package basic.hw2.part2;

import java.util.Scanner;
import java.util.Arrays;

public class Task1 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int n = input.nextInt();
        int m = input.nextInt();
        int[][] arr = new int[m][n];
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                arr[i][j] = input.nextInt();
            }
        }
        for (int i = 0; i < m; i++) {
            Arrays.sort(arr[i]);
        }

        int[] result = new int[m];
        for (int i = 0; i < m; i++) {
            result[i] = arr[i][0];
        }
        for (int e : result) {
            System.out.print(e + " ");
        }
    }
}
