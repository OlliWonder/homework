package basic.hw2.part2;

import java.util.Scanner;
public class Task3 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int n = input.nextInt() + 4;
        char[][] arr = new char[n][n];
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                arr[i][j] = '0';
            }
        }
        int x = input.nextInt() + 2;
        int y = input.nextInt() + 2;

        arr[y][x] = 'K';
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                arr[y + 2][x + 1] = 'X';
                arr[y + 2][x - 1] = 'X';
                arr[y + 1][x + 2] = 'X';
                arr[y + 1][x - 2] = 'X';
                arr[y - 1][x + 2] = 'X';
                arr[y - 1][x - 2] = 'X';
                arr[y - 2][x + 1] = 'X';
                arr[y - 2][x - 1] = 'X';
            }
        }
        for (int i = 2; i < n - 2; i++) {
            for (int j = 2; j < n - 2; j++) {
                System.out.print(j != n - 3 ? arr[i][j] + " " : arr[i][j]);
            }
            System.out.println();
        }
    }
}