package basic.hw2.part2;

import java.util.Scanner;

public class Task9 {
    public static void main(String[] args) {
        int n = new Scanner(System.in).nextInt();
        System.out.println(getNumbers(n));
    }
    public static String getNumbers(int n) {
        if (n < 10) {
            return Integer.toString(n);
        }
        else {
            return getNumbers(n / 10) + " " + n % 10;
        }
    }
}