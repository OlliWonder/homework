package basic.hw2.part2;

import java.util.Scanner;

public class Task2 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int n = input.nextInt();
        int[][] arr = new int[n][n];

        int x1 = input.nextInt();
        int y1 = input.nextInt();
        arr[y1][x1] = 1;

        int x2 = input.nextInt();
        int y2 = input.nextInt();
        arr[y2][x2] = 1;

        for (int i = y1; i <= y2; i++) {
            for (int j = x1; j <= x2; j++) {
                if (i > y1 && i < y2 && j > x1 && j < x2) {
                    continue;
                }
                else {
                    arr[i][j] = 1;
                }
            }
        }

        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                System.out.print(j != n -1 ? arr[i][j] + " " : arr[i][j]);
            }
            System.out.println();
        }
    }
}