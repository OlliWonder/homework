package basic.hw2.part2;

import java.util.Scanner;

public class Task5 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int n = input.nextInt();
        int[][] arr = new int[n][n];
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                arr[i][j] = input.nextInt();
            }
        }
        for (int i = 0; i < n - 1; i++) {
            for (int j = 0; j < n - 1; j++) {
                if (arr[i][j] == arr[n - 1 - j][n - 1 - i]) {
                    continue;
                }
                else {
                    System.out.println(false);
                    return;
                }
            }
        }
        System.out.println(true);

//          1  2  3   1  2  3  4
//          3  5  2   5  6  7  3
//          6  3  1   8  1  6  2
//                    2  8  5  1

    }
}
