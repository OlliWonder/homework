package basic.hw2.part2;

import java.util.Scanner;

public class Task10 {
    public static void main(String[] args) {
        int n = new Scanner(System.in).nextInt();
        System.out.println(getReversNumbers(n));
    }
    public static int getReversNumbers(int n) {
        if (n < 10) {
            return n;
        }
        else {
            System.out.print(n % 10 + " ");
            return getReversNumbers(n / 10);
        }
    }
}