package basic.hw2.part2;

import java.util.Scanner;

public class Task8 {
    public static void main(String[] args) {
        int n = new Scanner(System.in).nextInt();
        System.out.println(getSum(n));
    }
    public static int getSum(int n) {
        if (n / 10 == 0) {
            return n;
        }
        else {
            return n % 10 + getSum(n / 10);
        }
    }
}