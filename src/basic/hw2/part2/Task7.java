package basic.hw2.part2;

import java.util.Scanner;

public class Task7 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int n = input.nextInt();

        String[] ownersName = new String[n];
        for (int i = 0; i<n; i++) {
            ownersName[i] = input.next();
        }

        String[] dogsNick = new String[n];
        for (int i = 0; i<n; i++) {
            dogsNick[i] = input.next();
        }

        double[][] score = new double[n][3];
        for (int i = 0; i<n; i++) {
            for (int j = 0; j<3; j++) {
                score[i][j] = input.nextDouble();
            }
        }

        double[] scorePerDogs = new double[n];
        for (int i = 0; i<n; i++) {
            for (int j = 0; j<3; j++) {
                scorePerDogs[i] += score[i][j] / 3;
            }
        }

        double temp = scorePerDogs[0];
        int index = 0;
        for (int i = 1; i<n; i++) {
            if (temp<scorePerDogs[i]) {
                temp = scorePerDogs[i];
                index = i;
            }
        }

        double temp2 = scorePerDogs[0];
        int index2 = 0;
        for (int i = 1; i<n; i++) {
            if (temp2<scorePerDogs[i] && i != index) {
                temp2 = scorePerDogs[i];
                index2 = i;
            }
        }

        double temp3 = scorePerDogs[0];
        int index3 = 0;
        for (int i = 1; i<n; i++) {
            if (temp3<scorePerDogs[i] && i != index && i != index2) {
                temp3 = scorePerDogs[i];
                index3 = i;
            }
        }
        System.out.println(ownersName[index] + ": " + dogsNick[index] + ", " + (int) (scorePerDogs[index] * 10) / 10.0);
        System.out.println(ownersName[index2] + ": " + dogsNick[index2] + ", " + (int) (scorePerDogs[index2] * 10) / 10.0);
        System.out.println(ownersName[index3] + ": " + dogsNick[index3] + ", " + (int) (scorePerDogs[index3] * 10) / 10.0);
    }
}