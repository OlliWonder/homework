package basic.hw2.part1;

import java.util.Scanner;

public class Task11 {
    // Минимальная длина пароля
    public static final int MIN_PASSWORD_LENGTH = 8;
    
    public static void main(String[] args) {
        generatePassword();
    }
    
    /**
     * Метод получает символы и собирает их в пароль, удовлетворяющий следующим условиям:
     * есть заглавные латинские символы;
     * есть строчные латинские симолы;
     * есть числа;
     * есть специальные знаки (_*-).
     */
    private static void generatePassword() {
        Scanner input = new Scanner(System.in);
    
        // Выводим название программы.
        System.out.println("Программа для генерации безопасного пароля");
    
        int n; // Длина пароля
    
        // Предлагаем ввести длину пароля, считываем n, повторяем до тех пор, пока n < m.
        while (true) {
            System.out.print("\nВведите желаемую длину пароля: ");
            n = input.nextInt();
            if (n >= MIN_PASSWORD_LENGTH) {
                break;
            }
            System.out.println("Пароль с " + n + " количеством символов небезопасен");
        }
        // Создаём массив симолов для пароля
        char[] forPassword = new char[n];
        
        // Заполняем массив, используя методы для получения рандомных символов
        for (int i = 0, switcher = 0; i < n; i++, switcher++) {
            // Используем switcher, чтобы каждый символ гарантированно попал в пароль
            if (switcher == 0) {
                forPassword[i] = getUpperCaseLetter();
            }
            else if (switcher == 1) {
                forPassword[i] = getLowerCaseLetter();
            }
            else if (switcher == 2) {
                forPassword[i] = getNumber();
            }
            else {
                forPassword[i] = getSymbol();
                switcher = -1; // сбрасываем счётчик, чтобы заполнять опять с нуля.
            }
        }
        // Рандомно переставим символы пароля для большей рандомности :)
        for (int i = 0; i < n; i++) {
            int j = (int)(Math.random() * n);
            char temp = forPassword[i];
            forPassword[i] = forPassword[j];
            forPassword[j] = temp;
        }
        // Преобразуем массив char в строку и выведем на экран
        String password = new String(forPassword);
        System.out.println("Ваш пароль: " + password);
    }
    
    /**
     * Метод возвращает рандомную заглавную букву
     */
    private static char getUpperCaseLetter() {
        char[] letters = new char[26];
        for (int i = 0; i < 26; i++) {
            letters[i] = (char)(65 + i);
        }
        return letters[(int)(Math.random() * letters.length)];
    }
    
    /**
     * Метод возвращает рандомную строчную букву
     */
    private static char getLowerCaseLetter() {
        char[] letters = new char[26];
        for (int i = 0; i < 26; i++) {
            letters[i] = (char)(97 + i);
        }
        return letters[(int)(Math.random() * letters.length)];
    }
    
    /**
     * Метод возвращает рандомную цифру
     */
    private static char getNumber() {
        char[] numbers = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9'};
        return numbers[(int)(Math.random() * numbers.length)];
    }
    
    /**
     * Метод вощвращает рандомный символ
     */
    private static char getSymbol() {
        char[] symbols = {'_', '*', '-'};
        return symbols[(int)(Math.random() * symbols.length)];
    }
}