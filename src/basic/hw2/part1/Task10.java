package basic.hw2.part1;

import java.util.Scanner;

public class Task10 {
    public static void main(String[] args) {
        gameWithNumbers();
    }
    
    /**
     * Метод сравнивает введенное пользователем число с "загаданным" компьютером (с помощью random())
     * и выводит на экран результат сравнения до тех пор, пока они не совпадут.
     */
    private static void gameWithNumbers() {
        Scanner input = new Scanner(System.in);
        
        // Компьютер создаёт рандомное целое число от о до 1000 включительно.
        int m = (int)(Math.random() * 1000 + 1);
        System.out.println("Угадайте число, которое я загадал!");
        
        int n; // Число пользователя.
        
        // Предлагаем ввести число и считываем n до тех пор, пока оно положительное и не равное m.
        while (true) {
            System.out.print("""
        
                    Введите целое число от 0 до 1000 включительно
                    (для выхода из игры введите любое отрицательное число):\s""");
            n = input.nextInt();
            if (n < 0) {
                break;
            }
            else if (n == m) {
                System.out.println("Победа!");
                break;
            }
            else if (n > m) {
                System.out.println("Это число больше загаданного");
            }
            else {
                System.out.println("Это число меньше загаданного");
            }
        }
        System.out.println("Конец игры :)");
    }
}