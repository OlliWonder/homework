package basic.hw2.part1;

import java.util.Scanner;

public class Task1 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int n = input.nextInt();
        double[] list = new double[n];
        for (int i = 0; i < list.length; i++) {
            list[i] = input.nextDouble();
        }
        System.out.println(getAverage(list));
    }
    public static double getAverage(double[] list) {
        double sum = 0;
        for (double e: list) {
            sum += e;
        }
        return sum / list.length;
    }
}