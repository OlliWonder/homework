package basic.hw2.part1;

import java.util.Scanner;

public class Task12 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        // Создадим масси и заполним его входящими значениями
        int[] list = new int[input.nextInt()];
        for (int i = 0; i < list.length; i++) {
            list[i] = input.nextInt();
        }
        // Создадим массив, состоящий из элементов первого массисва,
        // возведенных в квадрат
        int[] res = new int[list.length];
        for (int i = 0; i < list.length; i++) {
            res[i] = (int)(Math.pow(list[i], 2));
        }
    
        // Найдём минимальное и максимальное значение в массиве
        int min = res[0];
        int max = res[0];
        for (int i = 1; i < res.length; i++) {
            if (res[i] < min) {
                min = res[i];
            }
            if (res[i] > max) {
                max = res[i];
            }
        }
        // Вызовем метод для сортировки массива
        countingSort(res, min, max);
        
        // Напечатаем отсортированный массив
        for (int i = 0; i < res.length; i++) {
            System.out.print(res[i] + " ");
        }
    }
    
    /**
     * Метод выполняет сортировку массива подсчётом элементовю.
     * Его временная сложность линейна и равна O(n + max), где max - максимальное число в исходном массиве.
     */
    private static void countingSort(int[] arr, int min, int max) {
        int[] count = new int[max - min + 1];
        // Индекс элемента массива count равен числу из исходного массива, а значение элемента под этим индексом равно
        // количеству повторений этого числа в исходном массиве
        for (int i = 0; i < arr.length; i++) {
            count[arr[i] - min]++;
        }
        // Делаем операцию наоборот, и записываем индексы массива count (по порядку) в значения элементов исходного
        // массива с учётом повторений
        int index = 0;
        for (int i = 0; i < count.length; i++) {
            for (int j = 0; j < count[i]; j++) {
                arr[index++] = i + min;
            }
        }
    }
}