package basic.hw2.part1;

import java.util.Scanner;

public class Task5 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int n = input.nextInt();
        int[] list = new int[n];
        for (int i = 0; i < list.length; i++) {
            list[i] = input.nextInt();
        }
        int m = input.nextInt();
        int[] result = new int[n];

        for (int i = list.length - 1; i >= 0; i--) {
            if (i + m >= list.length){
                result[i + m - list.length] = list[i];
            }
            else {
                result[i + m] = list[i];
            }
        }
        for (int e : result) {
            System.out.print(e == result[result.length - 1] ? e : e + " ");
        }
    }
}