package basic.hw2.part1;

import java.util.Scanner;

public class Task6 {
    public static void main(String[] args) {
        String s = new Scanner(System.in).nextLine();
        toMorse(s);
    }
    public static void toMorse(String s) {
        String[] morseCode = {".-", "-...", ".--", "--.", "-..", ".", "...-", "--..", "..", ".---", "-.-", ".-..",
                "--", "-.", "---", ".--.", ".-.", "...", "-", "..-", "..-.", "....", "-.-.", "---.", "----", "--.-",
                "--.--", "-.--", "-..-", "..-..", "..--", ".-.-"};

        String[] alphaBet = {"А", "Б", "В", "Г", "Д", "Е", "Ж", "З", "И", "Й", "К", "Л", "М", "Н", "О", "П", "Р",
                "С", "Т", "У", "Ф", "Х", "Ц", "Ч", "Ш", "Щ", "Ъ", "Ы", "Ь", "Э", "Ю", "Я"};

        String[] inputS = new String[s.length()];
        for (int i = 0; i < s.length(); i++) {
            inputS[i] = s.substring(i, i + 1);
        }

        for (int i = 0; i < inputS.length; i++) {
            for (int j = 0; j < alphaBet.length; j++) {
                if (inputS[i].equals(alphaBet[j])) {
                    System.out.print(i != inputS.length - 1 ? morseCode[j] + " " : morseCode[j]);
                }
            }
        }
    }
}