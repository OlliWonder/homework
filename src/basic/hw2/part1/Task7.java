package basic.hw2.part1;

import java.util.Scanner;
import java.util.Arrays;

public class Task7 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int[] list = new int[input.nextInt()];
        for (int i = 0; i<list.length; i++) {
            list[i] = input.nextInt();
        }

        int[] middle = new int[list.length];
        for (int i = 0; i<list.length; i++) {
            middle[i] = (int) Math.pow(list[i], 2);
        }

        Arrays.sort(middle);
        for (int e : middle) {
            System.out.print(e + " ");
        }
    }
}