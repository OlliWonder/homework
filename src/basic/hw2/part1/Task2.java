package basic.hw2.part1;

import java.util.Scanner;

public class Task2 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int n = input.nextInt();
        int[] listN = new int[n];
        for (int i = 0; i < listN.length; i++) {
            listN[i] = input.nextInt();
        }
        int m = input.nextInt();
        int[] listM = new int[m];
        for (int i = 0; i < listM.length; i++) {
            listM[i] = input.nextInt();
        }
        if (n == m) {
            for (int i = 0, j = 0; i < listN.length; i++, j++) {
                if (listN[i] == listM[j]) {
                    continue;
                }
                else {
                    System.out.println(false);
                    return;
                }
            }
            System.out.println(true);
        }
        else {
            System.out.println(false);
        }
    }
}