package basic.hw2.part1;

import java.util.Scanner;

public class Task3 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int n = input.nextInt();
        int[] list = new int[n];
        for (int i = 0; i < n; i++) {
            list[i] = input.nextInt();
        }
        int x = input.nextInt();

        int low = 0;
        int high = list.length - 1;
        while (high >= low) {
            int mid = (low + high) / 2;
            if (x == list[mid]) {
                for (int i = mid; i < list.length; i++) {
                    if (x == list[mid]) {
                        mid++;
                    }
                }
                System.out.println(mid);
                return;
            }
            else if (x < list[mid]) {
                high = mid - 1;
            }
            else {
                low = mid + 1;
            }
        }
        System.out.println(low);
    }
}