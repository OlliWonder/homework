package basic.hw2.part1;

import java.util.Scanner;

public class Task9 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        String[] text = new String[input.nextInt()];
        for (int i = 0; i < text.length; i++) {
            text[i] = input.next();
        }
        int count = 0;
        for (String s : text) {
            ++count;
            for (int i = count; i < text.length; i++) {
                if (s.equals(text[i])) {
                    System.out.println(s);
                    return;
                }
            }
        }
    }
}