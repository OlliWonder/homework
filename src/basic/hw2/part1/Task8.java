package basic.hw2.part1;

import java.util.Arrays;
import java.util.Scanner;

public class Task8 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int[] list = new int[input.nextInt()];
        for (int i = 0; i < list.length; i++) {
            list[i] = input.nextInt();
        }

        int m = input.nextInt();

        Arrays.sort(list);
        int index = Arrays.binarySearch(list, m);

        if (index >= 0) {
            System.out.println(list[index]);
        }
        else if (index == -1) {
            System.out.println(list[0]);
        }
        else if (index == -(list.length + 1)) {
            System.out.println(list[list.length - 1]);
        }
        else {
            int res1 = Math.abs(list[-index - 2] - m);
            int res2 = Math.abs(list[-index - 1] - m);
            if (res1 > res2) {
                System.out.println(list[-index - 1]);
            }
            else if (res1 < res2) {
                System.out.println(list[-index - 2]);
            }
            else {
                System.out.println(Math.max(list[-index - 2], list[-index - 1]));
            }
        }
    }
}