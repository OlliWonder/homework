package basic.hw1.part3;
import java.util.Scanner;

public class Task6 {
    public static void main(String[] args) {
        int n = new Scanner(System.in).nextInt();
        int remain = 0;
        for (int i = 8; i >= 1; i /= 2) {
            remain = n / i;
            n %= i;
            System.out.print(i == 1 ? remain : remain + " ");
        }
    }
}
