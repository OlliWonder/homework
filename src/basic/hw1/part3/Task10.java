package basic.hw1.part3;
import java.util.Scanner;

public class Task10 {
    public static void main(String[] args) {
        int n = new Scanner(System.in).nextInt();
        String space = " ";
        int count;
        for (int i = 0; i <= n; i++) {
            if (i < n) {
                count = n - i;
                for (int j = 1; j < count; j++) {
                    System.out.print(space);
                }
                for (int k = 0; k <= i * 2; k++) {
                    System.out.print("#");
                }
                System.out.print("\n");
            }
            else {
                count = n;
                for (int l = 1; l < count; l++) {
                    System.out.print(space);
                }
            }
        }
        System.out.print("|");
    }
}