package basic.hw1.part3;
import java.util.Scanner;

public class Task9 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int count = 0;
        while (input.hasNext()) {
            int a = input.nextInt();
            if (a < 0) {
                count++;
            }
            else {
                break;
            }
        }
        System.out.println(count);
    }
}