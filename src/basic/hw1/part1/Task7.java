package basic.hw1.part1;

import java.util.Scanner;

public class Task7 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int n = input.nextInt();
        int tens = n / 10;
        int ones = n % 10;
        System.out.println(ones + "" + tens);
    }
}
