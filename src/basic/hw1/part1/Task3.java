package basic.hw1.part1;

import java.util.Scanner;

public class Task3 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        String userName = input.nextLine();
        System.out.println("Привет, " + userName + "!");
    }
}
