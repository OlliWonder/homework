package basic.hw1.part1;

import java.util.Scanner;

public class Task1 {
    public static void main(String [] args) {
        Scanner input = new Scanner(System.in);
        int radius = input.nextInt();
        double value = 4.0 / 3 * Math.PI * Math.pow(radius, 3);
        System.out.println(value);
    }
}
