package basic.hw1.part1;

import java.util.Scanner;

public class Task6 {
    public static final double KILOMETERS_PER_MILE = 1.60934;
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int count = input.nextInt();
        double miles = count / KILOMETERS_PER_MILE;
        System.out.println(miles);
    }
}
