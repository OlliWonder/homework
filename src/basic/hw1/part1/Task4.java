package basic.hw1.part1;

import java.util.Scanner;

public class Task4 {
    public static final int SECONDS_PER_MINUTE = 60;
    public static final int MINUTES_PER_HOUR = 60;
    public static final int HOURS_PER_DAY = 24;
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int count = input.nextInt();
        int totalMinutes = count / SECONDS_PER_MINUTE;
        int currentMinutes = totalMinutes % MINUTES_PER_HOUR;
        int totalHours = totalMinutes / MINUTES_PER_HOUR;
        int currentHours = totalHours % HOURS_PER_DAY;
        System.out.println(currentHours + " " + currentMinutes);
    }
}
