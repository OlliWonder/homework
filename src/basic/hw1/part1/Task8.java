package basic.hw1.part1;

import java.util.Scanner;

public class Task8 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        double n = input.nextDouble();
        double budgetPerDay = n / 30;
        System.out.println(budgetPerDay);
    }
}
