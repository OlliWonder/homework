package basic.hw1.part1;

import java.util.Scanner;

public class Task5 {
    public static final double CENTIMETERS_PER_INCH = 2.54;
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int inch = input.nextInt();
        double centimeters = inch * CENTIMETERS_PER_INCH;
        System.out.println(centimeters);
    }
}
