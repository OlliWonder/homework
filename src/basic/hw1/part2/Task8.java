package basic.hw1.part2;

import java.util.Scanner;

public class Task8 {
    public static void main(String[] args) {
        String originalLine, firstLine, secondLine;
        int lastSpace;

        Scanner input = new Scanner(System.in);
        originalLine = input.nextLine();

        lastSpace = originalLine.lastIndexOf(' ');
        firstLine = originalLine.substring(0, lastSpace);
        secondLine = originalLine.substring(lastSpace + 1);

        System.out.println(firstLine);
        System.out.println(secondLine);
    }
}