package basic.hw1.part2;

import java.util.Scanner;

public class Task3 {
    public static void main(String[] args) {
        int x;

        Scanner input = new Scanner(System.in);
        x = input.nextInt();

        System.out.println(x > 12 ? "Пора" : "Рано");
    }
}
