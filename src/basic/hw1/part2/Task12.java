package basic.hw1.part2;

import java.util.Scanner;

public class Task12 {
    public static void main(String[] args) {
        String password;
        int length;
        boolean isUpperLetter, isLowerLetter, isNumber, isSymbol;

        Scanner input = new Scanner(System.in);
        password = input.nextLine();

        length = password.length();
        isUpperLetter = password.matches(".*[A-Z].*");
        isLowerLetter = password.matches(".*[a-z].*");
        isNumber = password.matches(".*\\d.*");
        isSymbol = password.matches(".*[_*-].*");

        if ((length >= 8) && isUpperLetter && isLowerLetter && isNumber && isSymbol) {
            System.out.println("пароль надежный");
        }
        else {
            System.out.println("пароль не прошел проверку");
        }
    }
}