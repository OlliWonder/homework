package basic.hw1.part2;

import java.util.Scanner;

public class Task5 {
    public static void main(String[] args) {
        int a, b, c;
        double discriminant;

        Scanner input = new Scanner(System.in);
        a = input.nextInt();
        b = input.nextInt();
        c = input.nextInt();

        discriminant = Math.pow(b, 2) - 4 * a * c;

        if (discriminant >= 0) {
            System.out.println("Решение есть");
        }
        else {
            System.out.println("Решения нет");
        }
    }
}