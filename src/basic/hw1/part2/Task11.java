package basic.hw1.part2;

import java.util.Scanner;

public class Task11 {
    public static void main(String[] args) {
        int a, b, c, sum;
        double difference;

        Scanner input = new Scanner(System.in);
        a = input.nextInt();
        b = input.nextInt();
        c = input.nextInt();

        sum = a + b + c;
        difference = sum / 2.0 - Math.max(Math.max(a, b), c);
        System.out.println(difference > 0);
    }
}