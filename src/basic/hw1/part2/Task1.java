package basic.hw1.part2;

import java.util.Scanner;

public class Task1 {
    public static void main(String[] args) {
        int a, b, c;

        Scanner input = new Scanner(System.in);
        a = input.nextInt();
        b = input.nextInt();
        c = input.nextInt();

        if (a > b && b > c) {
            System.out.println("Петя, пора трудиться");
        }
        else {
            System.out.println("Петя молодец!");
        }
    }
}
