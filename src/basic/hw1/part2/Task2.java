package basic.hw1.part2;

import java.util.Scanner;

public class Task2 {
    public static void main(String[] args) {
        int x, y;

        Scanner input = new Scanner(System.in);
        x = input.nextInt();
        y = input.nextInt();

        System.out.println(x > 0 && y > 0);
    }
}
