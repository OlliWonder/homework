package basic.hw1.part2;

import java.util.Scanner;

public class Task14 {

    public static final int MIN_PRICE = 50000;
    public static final int MAX_PRICE = 120000;

    public static void main(String[] args) {
        String model;
        int price, model1, model2;

        Scanner input = new Scanner(System.in);
        model = input.nextLine();
        price = input.nextInt();

        model1 = model.indexOf("samsung");
        model2 = model.indexOf("iphone");

        if ((model1 > -1 || model2 > -1) && (price >= MIN_PRICE && price <= MAX_PRICE)) {
            System.out.println("Можно купить");
        }
        else {
            System.out.println("Не подходит");
        }
    }
}