package basic.hw1.part2;

import java.util.Scanner;

public class Task13 {
    public static void main(String[] args) {
        String mailPackage;
        int rocks, denied;

        Scanner input = new Scanner(System.in);
        mailPackage = input.nextLine();

        rocks = mailPackage.indexOf("камни!");
        denied = mailPackage.indexOf("запрещенная продукция");

        if (rocks > -1) {
            if (denied > -1) {
                System.out.println("в посылке камни и запрещенная продукция");
            }
            else {
                System.out.println("камни в посылке");
            }
        }
        else if (rocks == -1) {
            if (denied == -1) {
                System.out.println("все ок");
            }
            else {
                System.out.println("в посылке запрещенная продукция");
            }
        }
    }
}