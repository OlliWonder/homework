package basic.hw1.part2;

import java.util.Scanner;

public class Task7 {
    public static void main(String[] args) {
        String originalLine, firstLine, secondLine;
        int firstSpace;

        Scanner input = new Scanner(System.in);
        originalLine = input.nextLine();

        firstSpace = originalLine.indexOf(' ');
        firstLine = originalLine.substring(0, firstSpace);
        secondLine = originalLine.substring(firstSpace + 1);

        System.out.println(firstLine);
        System.out.println(secondLine);
    }
}