package basic.hw1.part2;

import java.util.Scanner;

public class Task10 {
    public static final double EPSILON = 1E-14;

    public static void main(String[] args) {
        double n, identity;

        Scanner input = new Scanner(System.in);
        n = input.nextDouble();

        identity = Math.log(Math.exp(n));
        System.out.println(Math.abs(identity - n) < EPSILON);
    }
}