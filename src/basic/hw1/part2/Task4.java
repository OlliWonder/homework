package basic.hw1.part2;

import java.util.Scanner;

public class Task4 {
    public static final int NUMBER_OF_SATURDAY = 6;

    public static void main(String[] args) {
        int dayOfWeek, daysToSaturday;

        Scanner input = new Scanner(System.in);
        dayOfWeek = input.nextInt();

        if (dayOfWeek >= 1 && dayOfWeek < 6) {
            daysToSaturday = NUMBER_OF_SATURDAY - dayOfWeek;
            System.out.println(daysToSaturday);
        }
        else {
            System.out.println("Ура, выходные!");
        }
    }
}