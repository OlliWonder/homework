package basic.hw1.part2;

import  java.util.Scanner;

public class Task9 {
    public static final double EPSILON = 1E-14;

    public static void main(String[] args) {
        double x, identity;

        Scanner input = new Scanner(System.in);
        x = input.nextDouble();

        identity = Math.pow(Math.sin(x), 2) + Math.pow(Math.cos(x), 2);
        System.out.println(Math.abs(identity - 1.0) < EPSILON);
    }
}