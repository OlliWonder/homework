package basic.hw1.part2;

import java.util.Scanner;

public class TaskProbaPera {
    public static void main(String[] args) {
        String password;
        int length;
        boolean isUpperLetter, isLowerLetter, isNumber;
        char ch, symbol1 = '_', symbol2 = '*', symbol3 = '-';
        int count1 = 0, count2 = 0, count3 = 0, count4 = 0;

        Scanner input = new Scanner(System.in);
        password = input.nextLine();
        length = password.length();

        for (int i = 0; i < length; i++) {
            ch = password.charAt(i);
            isUpperLetter = Character.isUpperCase(ch);
            if (isUpperLetter) {
                break;
            }
            else {
                count1++;
            }
        }
        for (int i = 0; i < length; i++) {
            ch = password.charAt(i);
            isLowerLetter = Character.isLowerCase(ch);
            if (isLowerLetter) {
                break;
            }
            else {
                count2++;
            }
        }
        for (int i = 0; i < length; i++) {
            ch = password.charAt(i);
            isNumber = Character.isDigit(ch);
            if (isNumber) {
                break;
            }
            else {
                count3++;
            }
        }
        for (int i = 0; i < length; i++) {
            ch = password.charAt(i);
            if (ch == symbol1 || ch == symbol2 || ch == symbol3) {
                break;
            }
            else {
                count4++;
            }
        }
        if (length >= 8 && count1 < length && count2 < length && count3 < length && count4 < length) {
            System.out.println("пароль надежный");
        }
        else {
            System.out.println("пароль не прошел проверку");
        }
    }
}