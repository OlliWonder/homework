package pro.hw4;

import java.util.Scanner;

public class TaskDop {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        String s1 = input.nextLine();
        String s2 = input.nextLine();
        System.out.println(myComparing(s1, s2));
    }
    
    public static boolean myComparing(String s1, String s2) {
        int count = 0;
        if (s2.length() - s1.length() == 1) {
            for (int i = 0, j = 0; i < s1.length() && j < s2.length(); i++, j++) {
                if (s1.charAt(i) != s2.charAt(j)) {
                    count++;
                    j++;
                }
            }
            return count <= 1;
        }
    
        if (s1.length() - s2.length() == 1) {
            for (int i = 0, j = 0; i < s1.length() && j < s2.length(); i++, j++) {
                if (s1.charAt(i) != s2.charAt(j)) {
                    count++;
                    i++;
                }
            }
            return count <= 1;
        }
        
        if (s1.length() == s2.length()) {
            for (int i = 0; i < s1.length(); i++) {
                if (s1.charAt(i) != s2.charAt(i)) {
                    count++;
                }
            }
            return count == 1;
        }
        return false;
    }
}
