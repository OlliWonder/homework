package pro.hw4;

import java.util.Scanner;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
// Не доделано, не могу придумать, как проверить ещё и последовательность символов со стримами..
public class TaskDopV2 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        String s1 = input.nextLine();
        String s2 = input.nextLine();

        Set<Character> set = IntStream.concat(s1.chars(), s2.chars())
                .mapToObj(c -> (char)c)
                .collect(Collectors.toSet());
        System.out.println(Math.abs(s1.length() - s2.length()) <= 1 &&
                set.size() - Math.min(s1.length(), s2.length()) <= 1);
    }
}
