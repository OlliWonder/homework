package pro.hw4;

import java.util.Comparator;
import java.util.List;

public class Task4 {
    public static void main(String[] args) {
        List<Double> list = List.of(1.15, 3.14, 2.181, 9.88);
        list.stream()
                .sorted(Comparator.reverseOrder())
                .forEach(System.out::println);
    }
}
