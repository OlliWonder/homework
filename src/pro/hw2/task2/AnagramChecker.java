package pro.hw2.task2;

import java.util.Arrays;
import java.util.Scanner;

public class AnagramChecker {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        String s = input.nextLine();
        String t = input.nextLine();
        System.out.println(isAnagram(s, t));
    }
    
    private static boolean isAnagram(String s, String t) {
        char[] fromS = s.toLowerCase().toCharArray();
        Arrays.sort(fromS);
        char[] fromT = t.toLowerCase().toCharArray();
        Arrays.sort(fromT);
        return Arrays.equals(fromS, fromT);
    }
}
