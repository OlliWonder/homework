package pro.hw2.task1;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

public class UniqueList {
    public static void main(String[] args) {
        ArrayList<String> list = new ArrayList<>();
        list.add("How");
        list.add("do");
        list.add("you");
        list.add("do");
        list.add("How");
        list.add("are");
        list.add("you");
        System.out.println(getUnique(list));
        
        ArrayList<Integer> list2 = new ArrayList<>();
        list2.add(0);
        list2.add(0);
        list2.add(1);
        list2.add(2);
        list2.add(2);
        System.out.println(getUnique(list2));
    }
    
    private static <T> Set<T> getUnique(ArrayList<T> list) {
        return new HashSet<>(list);
    }
}
