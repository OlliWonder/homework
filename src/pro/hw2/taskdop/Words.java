package pro.hw2.taskdop;

import java.util.*;

public class Words {
    public static void main(String[] args) {
        String[] words = {"the","day","is","sunny","the","the","the",
                "sunny","is","is","day"};
        int k = 4;
        String[] result = countWords(words, k);
        System.out.println(Arrays.toString(result));
    }
    
    private static String[] countWords(String[] words, int k) {
        Map<String, Integer> pair = new TreeMap<>();
        for (String s : words) {
            int counter = 1;
            if (pair.containsKey(s)) {
                counter = pair.get(s);
                counter++;
            }
            pair.put(s, counter);
        }
        
        Map<String, Integer> sortedPair = sortByValues(pair);
        String[] values = sortedPair.keySet().toArray(new String[0]);
        String[] result = new String[k];
        System.arraycopy(values, 0, result, 0, k);
        return result;
    }
    
    private static <K, V extends Comparable<V>> Map<K, V> sortByValues(final Map<K, V> map) {
        Comparator<K> valueComparator =  new Comparator<K>() {
            public int compare(K k1, K k2) {
                int compare = map.get(k2).compareTo(map.get(k1));
                if (compare == 0) return -1;
                else return compare;
            }
        };
        Map<K, V> sortedByValues = new TreeMap<K, V>(valueComparator);
        sortedByValues.putAll(map);
        return sortedByValues;
    }
}
