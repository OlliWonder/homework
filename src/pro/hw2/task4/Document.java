package pro.hw2.task4;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Document {
    private final int id;
    private final String name;
    private final int pageCount;
    
    public Document(int id, String name, int pageCount) {
        this.id = id;
        this.name = name;
        this.pageCount = pageCount;
    }
    
    public int getId(Document document) {
        return id;
    }
    
    public String getName() {
        return name;
    }
    
    public int getPageCount() {
        return pageCount;
    }
    
    public static Map<Integer, Document> organizeDocuments(List<Document> documents) {
        Map<Integer, Document> result = new HashMap<>();
        for (Document e : documents) {
            result.put(e.getId(e), e);
        }
        return result;
    }
    
    @Override
    public String toString() {
        return name + ", " + pageCount + " стр.";
    }
}
