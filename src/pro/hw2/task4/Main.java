package pro.hw2.task4;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static pro.hw2.task4.Document.organizeDocuments;

public class Main {
    public static void main(String[] args) {
        Document document1 = new Document(1, "Договор", 5);
        Document document2 = new Document(2, "Списание", 1);
        Document document3 = new Document(3, "План", 3);
    
        List<Document> list = new ArrayList<>();
        list.add(document1);
        list.add(document2);
        list.add(document3);
        
        Map<Integer, Document> documentMap = organizeDocuments(list);
        System.out.println(documentMap);
    }
}
