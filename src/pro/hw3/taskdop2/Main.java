package pro.hw3.taskdop2;

import java.util.Deque;
import java.util.LinkedList;

public class Main {
    public static void main(String[] args) {
        System.out.println(isCorrectMultiBracketSequence(""));
        System.out.println(isCorrectMultiBracketSequence("{()[]()}"));
        System.out.println(isCorrectMultiBracketSequence("{)(}"));
        System.out.println(isCorrectMultiBracketSequence("[}"));
        System.out.println(isCorrectMultiBracketSequence("[{(){}}][()]{}"));
    }
    
    public static boolean isCorrectMultiBracketSequence(String s) {
        Deque<Character> stack = new LinkedList<>();
        char[] arr = s.toCharArray();
        for (char ch: arr) {
            if (ch == '{' || ch == '[' || ch == '(') {
                stack.addFirst(ch);
            }
            else {
                if (!stack.isEmpty() && ((stack.peekFirst() == '{' && ch == '}')
                        || (stack.peekFirst() == '[' && ch == ']') || (stack.peekFirst() == '(' && ch == ')'))) {
                    stack.removeFirst();
                }
                else {
                    return false;
                }
            }
        }
        return stack.isEmpty();
    }
}
