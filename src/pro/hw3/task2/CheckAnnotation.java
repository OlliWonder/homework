package pro.hw3.task2;

import pro.hw3.task1.IsLike;

public class CheckAnnotation {
    public static void main(String[] args) {
        Class<WithAnnotation> cls = WithAnnotation.class;
        checkAnnotationReflectively(cls);
        
        Class<WithoutAnnotation> cls2 = WithoutAnnotation.class;
        checkAnnotationReflectively(cls2);
    }
    
    public static void checkAnnotationReflectively(Class<?> cls) {
        System.out.println(cls.isAnnotationPresent(IsLike.class) ? cls.getAnnotation(IsLike.class) :
                "Аннотация @IsLike не используется");
    }
}
