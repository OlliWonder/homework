package pro.hw3.taskdop1;

public class Main {
    public static void main(String[] args) {
        System.out.println(isCorrectBracketSequence(""));
        System.out.println(isCorrectBracketSequence("(()()())"));
        System.out.println(isCorrectBracketSequence(")("));
        System.out.println(isCorrectBracketSequence("(()"));
        System.out.println(isCorrectBracketSequence("((()))"));
    }
    
    public static boolean isCorrectBracketSequence(String s) {
        if (s.equals("")) {
            return true;
        }
        int count = 0;
        char[] chars = s.toCharArray();
    
        for (char ch : chars) {
            if (ch == '(') {
                count++;
            }
            else if (ch == ')') {
                count--;
            }
            if (count < 0) {
                return false;
            }
        }
        return count == 0;
    }
}
