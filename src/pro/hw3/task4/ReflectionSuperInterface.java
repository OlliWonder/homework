package pro.hw3.task4;

import java.util.*;

public class ReflectionSuperInterface {
    public static void main(String[] args) {
        System.out.println(getAllInterfaces(MyClass.class));
    }
    
    public static Set<Class<?>> getAllInterfaces(Class<?> cls) {
        Set<Class<?>> interfaces = new HashSet<>();
        while (cls != Object.class) {
            Set<Class<?>> tmp = new HashSet<>(List.of(cls.getInterfaces()));
            for (Class<?> interfaceCls : tmp) {
                Class<?> checkedInterface = interfaceCls;
                while (checkedInterface != null) {
                    tmp.addAll(List.of(checkedInterface.getInterfaces()));
                    checkedInterface = checkedInterface.getSuperclass();
                }
            }
            interfaces.addAll(tmp);
            cls = cls.getSuperclass();
        }
        return interfaces;
    }
}
