package pro.hw1.task6;

public class Main {
    public static void main(String[] args) {
        try {
            FormValidator.checkName("Петя");
        } catch (InvalidNameException e) {
            System.out.println(e.getMessage());
        }
    
        try {
            FormValidator.checkName("ABas");
        } catch (InvalidNameException e) {
            System.out.println(e.getMessage());
        }
        
        try {
            FormValidator.checkBirthDate("27.05.1978");
        }
        catch (InvalidDateException e) {
            System.out.println(e.getMessage());
        }
        
        try {
            FormValidator.checkBirthDate("07.12.2023");
        }
        catch (InvalidDateException e) {
            System.out.println(e.getMessage());
        }
        
        try {
            FormValidator.checkGender("FEMALE");
        }
        catch (InvalidGenderException e) {
            System.out.println(e.getMessage());
        }
        
        try {
            FormValidator.checkGender("ABC");
        }
        catch (InvalidGenderException e) {
            System.out.println(e.getMessage());
        }
        
        try {
            FormValidator.checkHeight("180");
        }
        catch (InvalidHeightException e) {
            System.out.println(e.getMessage());
        }
    
        try {
            FormValidator.checkHeight("0");
        }
        catch (InvalidHeightException e) {
            System.out.println(e.getMessage());
        }
    }
}
