package pro.hw1.task6;

public class InvalidNameException extends Exception {
    public InvalidNameException() {
        super("имя указано неверно");
    }
    
    public InvalidNameException(String message) {
        super(message);
    }
}
