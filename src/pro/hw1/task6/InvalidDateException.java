package pro.hw1.task6;

public class InvalidDateException extends Exception {
    public InvalidDateException() {
        super("дата указана неверно");
    }
    
    public InvalidDateException(String message) {
        super(message);
    }
}
