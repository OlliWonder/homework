package pro.hw1.task6;

public class InvalidHeightException extends Exception {
    public InvalidHeightException() {
        super("рост указан неверно");
    }
    
    public InvalidHeightException(String message) {
        super(message);
    }
}
