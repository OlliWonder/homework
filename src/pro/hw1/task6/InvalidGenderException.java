package pro.hw1.task6;

public class InvalidGenderException extends Exception {
    public InvalidGenderException() {
        super("пол указан неверно");
    }
    
    public InvalidGenderException(String message) {
        super(message);
    }
}
