package pro.hw1.task6;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class FormValidator {
    private FormValidator() {
    }
    
    public static void checkName(String str) throws InvalidNameException {
        if (!str.matches("^(([A-Z][a-z]{1,19})|([А-Я][а-я]{1,19}))$")) {
            throw new InvalidNameException();
        }
    }
    
    public static void checkBirthDate(String str) throws InvalidDateException {
        if (!str.matches("(0[1-9]|[12][0-9]|3[01])\\.(0[1-9]|1[012])\\.((19|20)\\d\\d)")) {
            throw new InvalidDateException();
        }
    
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd.MM.yyyy");
        LocalDate birthday = LocalDate.parse(str, dtf);
        LocalDate endTime = LocalDate.now();
        LocalDate startTime = LocalDate.parse("01.01.1900", dtf);
        
        if (birthday.isBefore(startTime) || birthday.isAfter(endTime)) {
            throw new InvalidDateException();
        }
    }
    
    public static void checkGender(String str) throws InvalidGenderException {
        try {
            Gender.valueOf(str);
        }
        catch (IllegalArgumentException e) {
            throw new InvalidGenderException();
        }
    }
    
    public static void checkHeight(String str) throws InvalidHeightException {
        try {
            double height = Double.parseDouble(str);
            if (height <= 0) {
                throw new InvalidHeightException();
            }
        }
        catch (NumberFormatException | InvalidHeightException e) {
            throw new InvalidHeightException();
        }
    }
}
