package pro.hw1.task3;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.Scanner;
import static java.lang.Character.toUpperCase;

public class FileReadAndWrite {
    private static final String PKG_DIRECTORY = "D:\\Рабочий стол\\Учу java\\HomeWork1_1\\src\\pro\\hw1\\task3";
    private static final String INPUT_FILE_NAME = "input.txt";
    private static final String OUTPUT_FILE_NAME = "output.txt";
    
    public static void main(String[] args) {
        try {
            readAndWriteFile();
        }
        catch (IOException e) {
            System.out.println("FileReadWrite#main!error: " + e.getMessage());
        }
    }
    
    public static void readAndWriteFile() throws IOException {
        Scanner scanner = new Scanner(new File(PKG_DIRECTORY + "\\" + INPUT_FILE_NAME));
        Writer writer = new FileWriter(PKG_DIRECTORY + "\\" + OUTPUT_FILE_NAME);
        
        String temp;
        StringBuilder stringBuilder = new StringBuilder();
        
        try (scanner; writer) {
            while (scanner.hasNext()) {
                temp = scanner.nextLine();
                
                for (int i = 0; i < temp.length(); i++) {
                    if (temp.charAt(i) >= 97 && temp.charAt(i) <= 122) {
                        stringBuilder.append(toUpperCase(temp.charAt(i)));
                    }
                    else {
                        stringBuilder.append(temp.charAt(i));
                    }
                }
                writer.write(stringBuilder + "\n");
                stringBuilder.delete(0, stringBuilder.length());
            }
        }
    }
}
