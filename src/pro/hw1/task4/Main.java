package pro.hw1.task4;

public class Main {
    public static void main(String[] args) {
        try {
            MyEvenNumber myEvenNumber = new MyEvenNumber(0);
            System.out.println(myEvenNumber.getN());
            
            MyEvenNumber myEvenNumber2 = new MyEvenNumber(1);
            System.out.println(myEvenNumber2.getN());
        }
        catch (MyArithmeticException e) {
            System.out.println(e.getMessage());
        }
    }
}
