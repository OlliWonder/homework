package pro.hw1.task4;

public class MyArithmeticException extends ArithmeticException {
    public MyArithmeticException() {
        super("было введено нечётное число");
    }
    
    public MyArithmeticException(String message) {
        super(message);
    }
}
