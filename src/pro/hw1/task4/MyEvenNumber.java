package pro.hw1.task4;

public class MyEvenNumber {
    private final int n;
    
    public MyEvenNumber(int n) {
        if (n % 2 != 0) {
            throw new MyArithmeticException();
        }
        this.n = n;
    }
    
    public int getN() {
        return n;
    }
}
