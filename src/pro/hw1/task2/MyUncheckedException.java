package pro.hw1.task2;

public class MyUncheckedException extends ArithmeticException {
    
    public MyUncheckedException() {
        super();
    }
    
    public MyUncheckedException(String message) {
        super(message);
    }
}
