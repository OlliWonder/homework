package pro.hw1.taskdop2;
import java.util.Arrays;
import java.util.Scanner;
import java.util.ArrayList;

public class Main {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int n = input.nextInt();
        int[] arr = new int[n];
        for (int i = 0; i < n; i++) {
            arr[i] = input.nextInt();
        }
        int p = input.nextInt();
        System.out.println(Arrays.binarySearch(arr, p) < 0 ? -1 : Arrays.binarySearch(arr, p));
    }
}