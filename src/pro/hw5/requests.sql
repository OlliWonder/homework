--1. По идентификатору заказа получить данные заказа и данные клиента, создавшего этот заказ
select * from orders
join buyers on orders.buyer_id = buyers.id
and orders.id = 5;

--2. Получить данные всех заказов одного клиента по идентификатору клиента за последний месяц
select * from orders
where buyer_id = 1
and date_created > now() - interval '1 month';

--3. Найти заказ с максимальным количеством купленных цветов, вывести их название и количество
select fl.name, od.quantity
from flowers fl
join orders od on fl.id = od.flower_id
where od.quantity = (select max(quantity) from orders);

--4. Вывести общую выручку (сумму золотых монет по всем заказам) за все время
select sum(od.quantity * fl.price)
from orders od, flowers fl
where od.flower_id = fl.id;