create table orders
(
    id bigserial primary key,
    buyer_id integer references buyers(id),
    flower_id integer references flowers(id),
    quantity integer check(quantity >= 1 and quantity <= 1000),
    date_created timestamp
);

insert into orders (buyer_id, flower_id, quantity, date_created)
values (1, 2, 5, now() - interval '32 days');
insert into orders (buyer_id, flower_id, quantity, date_created)
values (2, 1, 99, now());
insert into orders (buyer_id, flower_id, quantity, date_created)
values (3, 3, 33, now());
insert into orders (buyer_id, flower_id, quantity, date_created)
values (1, 1, 9, now());
insert into orders (buyer_id, flower_id, quantity, date_created)
values (1, 3, 19, now());
insert into orders (buyer_id, flower_id, quantity, date_created)
values (2, 1, 5, now());

select * from orders;
commit;