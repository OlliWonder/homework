create table buyers
(
    id bigserial primary key,
    name varchar(30) not null,
    phone_number varchar(30) unique
);

insert into buyers (name, phone_number)
values ('Иван', '+7-918-012-34-54');
insert into buyers (name, phone_number)
values ('Петр', '+7-909-876-54-32');
insert into buyers (name, phone_number)
values ('Оксана', '+7-924-525-78-98');

select * from buyers;
commit;