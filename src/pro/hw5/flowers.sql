create table flowers
(
    id serial primary key,
    name varchar(30) unique,
    price integer not null
);

insert into flowers (name, price)
values ('Розы', 100);
insert into flowers (name, price)
values ('Лилии', 50);
insert into flowers (name, price)
values ('Ромашки', 25);

select * from flowers;
commit;