package pro.hw6;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class TaskDop1 {
    public static void main(String[] args) {
        String number = new Scanner(System.in).next();
        int m = number.length();
        int n = Integer.parseInt(number);
        
        List<Integer> list = new ArrayList<>();
        while (n > 0) {
            list.add(n % 10);
            n /= 10;
        }
        
        long sum = list.stream()
                .map(x -> (int) Math.pow(x, m))
                .reduce(0, (x1, x2) -> x1 + x2);
        System.out.println(Integer.parseInt(number) == sum);
    }
}
