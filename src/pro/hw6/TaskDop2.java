package pro.hw6;
import java.util.Scanner;
public class TaskDop2 {
    public static void main(String[] args) {
        int n = new Scanner(System.in).nextInt();
        int sqrt = (int)Math.ceil(Math.sqrt(n));
       
        if (n == 2) {
            System.out.println(true);
        }
        else if (n % 2 == 0 || n <= 1) {
            System.out.println(false);
        }
        else {
            for (int i = 3; i <= sqrt; i += 2) {
                if (n % i == 0) {
                    System.out.println(false);
                    return;
                }
            }
            System.out.println(true);
        }
    }
}